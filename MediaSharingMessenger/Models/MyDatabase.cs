﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace MediaSharingMessenger.Models
{
    public class MyDatabase: DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<UserOnline> UsersOnline { get; set; }
        public DbSet<UsersAndDialogs> UsersAndDialogs { get; set; }
        public DbSet<Dialog> Dialogs { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Contact> Contacts { get; set; }

        public MyDatabase() : base("MyDB")
        {
            Database.SetInitializer<MyDatabase>(new DropCreateDatabaseIfModelChanges<MyDatabase>());
        }
    }
}