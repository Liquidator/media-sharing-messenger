﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace MediaSharingMessenger.Models
{
    public class Contact
    {
        [Key]
        public int Id { get; set; }

        public int FrstUserId { get; set; }

        public int SecondUserId { get; set; }
    }
}