﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MediaSharingMessenger.Models
{
    public class UserOnline
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public int DialogId { get; set; }
    }
}