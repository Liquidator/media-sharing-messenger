﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MediaSharingMessenger.Models
{
    public class Message
    {
        [Key]
        public int Id { get; set; }
        public int DialogId { get; set; }
        public int UserId { get; set; }
        public DateTime Date { get; set; }
        public string Text { get; set; }

        public override string ToString() 
        {
            string s;
            using (var db = new MyDatabase()) 
            {
                var user = db.Users.FirstOrDefault<User>(new Func<User, bool>((u) => { return (u.Id == this.UserId); }));
                s = user.Name + ": " + this.Text + " | " + this.Date.ToShortDateString();
            }
            return s;
        }
    }
}