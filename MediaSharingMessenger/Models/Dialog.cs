﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MediaSharingMessenger.Models
{
    public class Dialog
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int MediaId { get; set; }
    }
}