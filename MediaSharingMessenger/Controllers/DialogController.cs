﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using MediaSharingMessenger.Models;

namespace MediaSharingMessenger.Controllers
{
    public class DialogController: Controller
    {
        [Authorize]
        private ActionResult Index(int id = 1)
        {
            IEnumerable<Message> messages;
            using (var db = new MyDatabase())
            {
                if(db.Dialogs.First<Dialog>((d) => d.Id == id) == null)
                    db.Dialogs.Add(new Dialog() { Id = id, Name = id.ToString() });

                messages = db.Messages.Select<Message, Message>(a => a).Where<Message>((msg) => msg.DialogId == id).ToList<Message>();

                ViewBag.Contacts = db.Contacts.Select<Contact, int>((cont, a) => 0);
            }

            ViewBag.Contacts = new User[] { new User() { Id = 1, Name = "Wasia" }, new User() { Id = 2, Name = "Petya" } };
            ViewBag.DialogId = id;

            return View(messages);
        }

        [Authorize]
        public ActionResult Index(string name = "1")
        {
            int id = 0;
            name = (string)Request.RequestContext.RouteData.Values["id"];
            using (var db = new MyDatabase())
            {
                var dialog = db.Dialogs.FirstOrDefault<Dialog>(d => d.Name == name);
                if (dialog != null)
                {
                    id = dialog.Id;
                }
                else 
                {
                    db.Dialogs.Add(new Dialog(){ Name = name });
                    db.SaveChanges();
                    id = db.Dialogs.First<Dialog>(d => d.Name == name).Id;
                }
                int uid = UserId();
                if ( db.UsersAndDialogs.FirstOrDefault<UsersAndDialogs>( a => (a.UserId == uid && a.DialogId == id) ) == null)
                    db.UsersAndDialogs.Add(new UsersAndDialogs() { DialogId = id, UserId = uid });
            }
            return Index(id);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Message(FormCollection fc)
        {
            
            Message msg;
            using (var db = new MyDatabase())
            {
                if (fc["message"][0] != '/')
                {
                    msg = GenerateAMessage(fc["message"], UserId());
                    db.Messages.Add(msg);
                    db.SaveChanges();
                }
                else 
                {
                    string [] command = fc["message"].Split(' ');
                    switch(command[0])
                    {
                        case "/dialog":
                        case "/room": 
                            {
                                if (command.Length < 2)
                                {
                                    msg = GenerateAMessage("Agrument missing: dialog_name", 1);
                                }
                                else if (command.Length > 2)
                                {
                                    msg = GenerateAMessage("Unknown argument: " + command[2], 1);
                                }
                                else 
                                {
                                    int uid = UserId();
                                    msg = GenerateAMessage(db.Users.First<User>(u => u.Id == uid).Name + " has left the dialog.", 1);
                                    ViewBag.Redirect = command[1];
                                }
                            } break;
                        case "/members": 
                            {
                                msg = GenerateAMessage("Not implemented yet.", 1); 
                            } break;
                        default: 
                            {
                                msg = GenerateAMessage("Unknown command: " + command[0], 1);
                            } break;
                    }
                }
            }            

            return PartialView(msg);
        }

        private int UserId() 
        {
            using (var db = new MyDatabase())
            {
                return db.Users.First<User>(u => u.Name == User.Identity.Name).Id;
            }
        }

        private Message GenerateAMessage(string Text, int UserId)
        {
            Message msg;

            msg = new Message() { Text = Text, Date = DateTime.Now, DialogId = Convert.ToInt32(Request.RequestContext.RouteData.Values["id"]), UserId = UserId };
            
            return msg;
        }
    }
}